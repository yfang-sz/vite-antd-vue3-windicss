import About from './views/About.vue';
import NotFound from './views/NotFound.vue';
import Layout from './layout/index.vue';

/** @type {import('vue-router').RouterOptions['routes']} */
export const routes = [
	{
		path: '/',
		component: Layout,
        children: [
            {
                path: '',
                component: () => import('./views/Home.vue')
            }
        ]
	},
	{
		path: '/about',
		meta: { title: 'About' },
		component: About
		// example of route level code-splitting
		// this generates a separate chunk (About.[hash].js) for this route
		// which is lazy-loaded when the route is visited.
		// component: () => import('./views/About.vue')
	},
	{ path: '/:path(.*)', component: NotFound }
];
