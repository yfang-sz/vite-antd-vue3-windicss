<p align='center'>
<img src="https://vitejs.dev/logo.svg" alt="vite" height="120" />
<img src="https://windicss.org/assets/logo.svg" alt="windicss" height="120" />
<img src="https://alicdn.antdv.com/v2/assets/logo.1ef800a8.svg" alt="antdv" height="120" />
</p>

<p align='center'>
Mocking up web app with <b>Vite</b><sup><em>(speed)</em></sup><br>
<sub><em>Now with <a href="http://windicss.netlify.app/">Windi CSS</a>! ⚡️</em></sub>
</p>

## Features

- ⚡️ [Vue 3](https://github.com/vuejs/vue-next), [Vite 2](https://github.com/vitejs/vite), [pnpm](https://pnpm.js.org/), [ESBuild](https://github.com/evanw/esbuild) - born with fastness

- 🗂 [File based routing](./src/layout)

- 📦 [Components auto importing](./src/views/Home.vue)

- 🎨 [Windi CSS](https://github.com/windicss/windicss) - next generation utility-first CSS framework

- 😃 [Use icons from any icon sets, with no compromise](./src/views/Home.vue)

- 🔥 Use the [new `<script setup>` style](https://github.com/vuejs/rfcs/pull/227)

<br>

## Pre-packed

### UI Frameworks

- [Windi CSS](https://github.com/windicss/windicss) (On-demand [TailwindCSS](https://tailwindcss.com/)) - lighter and faster, with a bundle additional features!
  - [Windi CSS Typography](https://windicss.org/plugins/official/typography.html) - similar to [Tailwind CSS Typography](https://github.com/tailwindlabs/tailwindcss-typography) but for Windi CSS
  - [Windi CSS Line Clamp](https://windicss.org/plugins/official/line-clamp.html)
- [ant design vue](https://2x.antdv.com/components/overview-cn/) - provides plenty of UI components to enrich your web applications, and we will improve components experience consistently.

### Icons

- [Iconify](https://iconify.design) - use icons from any icon sets [🔍Icônes](https://icones.netlify.app/)
- [unplugin-icons](https://github.com/antfu/unplugin-icons) - icons as Vue components

### Plugins

- [Vue Router](https://github.com/vuejs/vue-router)
- [unplugin-vue-components](https://github.com/antfu/unplugin-vue-components) - components auto import
- [vite-plugin-windicss](https://github.com/antfu/vite-plugin-windicss) - WindiCSS support

### Coding Style

- Use Composition API with [`<script setup>` SFC syntax](https://github.com/vuejs/rfcs/pull/227)
- [ESLint](https://eslint.org/) with [@antfu/eslint-config](https://github.com/antfu/eslint-config), single quotes, no semi.

### Dev tools

- [pnpm](https://pnpm.js.org/) - fast, disk space efficient package manager
- [VS Code Extensions](./.vscode/extensions.json)
  - [Vite](https://marketplace.visualstudio.com/items?itemName=antfu.vite) - Fire up Vite server automatically
  - [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar) - Vue 3 `<script setup>` IDE support
  - [Iconify IntelliSense](https://marketplace.visualstudio.com/items?itemName=antfu.iconify) - Icon inline display and autocomplete
  - [Windi CSS Intellisense](https://marketplace.visualstudio.com/items?itemName=voorjaar.windicss-intellisense) - IDE support for Windi CSS
  - [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

## Try it now!

### Clone to local

If you prefer to do it manually with the cleaner git history

```bash
git clone https://gitee.com/frank-fang-sz/vite-antd-vue3-windicss.git
cd vite-antd-vue3-windicss
pnpm i # If you don't have pnpm installed, run: npm install -g pnpm
```

## Checklist

When you use this template, try follow the checklist to update your info properly

- [ ] Rename `name` field in `package.json`
- [ ] Change the author name in `LICENSE`
- [ ] Change the title in `App.vue`
- [ ] Change the favicon in `public`
- [ ] Remove the `.github` folder which contains the funding info
- [ ] Clean up the READMEs and remove routes

And, enjoy :)

## Usage

### Development

Just run and visit http://localhost:3000

```bash
pnpm dev
```

### Build

To build the App, run

```bash
pnpm build
```

And you will see the generated file in `dist` that ready to be served.
