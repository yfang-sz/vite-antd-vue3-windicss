import { defineConfig } from 'vite-plugin-windicss';
import typography from 'windicss/plugin/typography';
import lineClamp from 'windicss/plugin/line-clamp';

export default defineConfig({
	plugins: [typography, lineClamp],
	attributify: true,
	theme: {}
});
