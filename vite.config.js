import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
// import vueJSX from '@vitejs/plugin-vue-jsx';
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { AntDesignVueResolver } from 'unplugin-vue-components/resolvers'
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'
import WindiCSS from 'vite-plugin-windicss';
// import StyleImport from 'vite-plugin-style-import';
import legacy from '@vitejs/plugin-legacy';
import path from 'path';

export default defineConfig({
    css: {
        preprocessorOptions: {
            less: {
                javascriptEnabled: true
            }
        }
    },
    resolve: {
		alias: {
			'@': path.resolve(__dirname, '/src')
		}
	},
	server: {
        port: process.env.port || 3000,
		open: false,
        proxy: {
			'^/micro': {
				// target: '',
				target: '',
				changeOrigin: true,
				rewrite: (path) => path.replace(/^\/micro/, '')
			}
		}
	},
	plugins: [
		vue(),
        AutoImport({
            imports: [
                'vue',
                'vue-router',
            ]
        }),
        /* StyleImport({
			libs: [
				{
					libraryName: 'ant-design-vue',
					esModule: true,
					resolveStyle: (name) => {
						return `ant-design-vue/es/${name}/style/index`;
					}
				}
			]
		}), */
		Components({
            resolvers: [
				IconsResolver({
					componentPrefix: 'icon'
				}),
                AntDesignVueResolver({
                    importStyle: 'less' /* 若要自行引入less，则可设置为 false */
                }),
                // VueUseComponentsResolver(),
                /* (name) => {
                    if (name.match(/^A[A-Z]/)) {
                        let importName = name.slice(1);
                        if (importName.indexOf('Layout') !== -1) {
                            importName = 'Layout';
                        }
                        const dirName = kebabCase(importName);
                        return {
                            importName,
                            path: 'ant-design-vue/es',
                            sideEffects: `ant-design-vue/es/${dirName}/style`
                        }
                    }
                } */
			]
		}),
		Icons({
            compiler: 'vue3'
        }),
        WindiCSS(),
		legacy()
	]
});
